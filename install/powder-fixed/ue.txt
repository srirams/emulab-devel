-- Install pre-requisites for 'tbadb_serv'

	sudo apt-get update
	sudo apt-get install libnet-server-perl libjson-perl unzip
	sudo apt-get install android-tools-adb android-tools-fastboot


-- Create a small LVM for images, we have some extra room.

	sudo mkdir /z
	sudo /sbin/lvcreate -n z -L 20G xen-vg
	sudo mke2fs -j /dev/xen-vg/z
	sudo mount /dev/xen-vg/z /z
	sudo mkdir /z/tbadb_img_cache
	echo '/dev/xen-vg/z /z ext3 defaults 0 0' | sudo csh -c '(cat >> /etc/fstab)'

-- Find UE IMSI by running on adb:

	adb shell service call iphonesubinfo 3

   which prints out hex format.
   

-- Find the UE Serial number:

	 adb devices

   which prints the serial number,

-- Create device mapping file using the serial number from above command.

	echo 'ue1 SERIAL' | sudo csh -c '(cat > /etc/emulab/tbadbmap)'

-- rsync over the emulab-devel source code to the control node to build
   tbadb.

	cd emulab-devel
	mkdir obj/clientside
	cd obj/clientside
	../../emulab-devel/clientside/configure --enable-mobile=yes \
	      --with-TBDEFS=../../emulab-devel/defs-utahclient
	cd mobile
	sudo make client-install
	cd ../tmcc/common
	sudo make path-install
	cd ../../../../emulab-devel/clientside/tmcc/common/
	sudo cp libtestbed.pm /usr/local/etc/emulab

-------

This was on tiny boss:

Add to tiny boss.

	# Mobile networking support (PhantomNet)
	MOBILESUPPORT=1
and rebuild/install

Import the phone image. While this has an .ndz extension, this is not an
ndz file and image_import eventually fails.

wap image_import -g -p emulab-ops 'https://www.emulab.net/image_metadata.php?uuid=c40718d8-e56f-11e5-b570-99cadac50270'
cd /usr/testbed/images/ANDROID444-STD/
mv ANDROID444-STD.ndz.new ANDROID444-STD.ndz    
mv ANDROID444-STD.ndz.new.sha1 ANDROID444-STD.ndz.sha1
imagevalidate -u -V size emulab-ops,ANDROID444-STD

wap editnodetype /tmp/nexus5.xml

wap addrfdevice -t nexus5 ue1

insert into tiplines (tipname, node_id, server) values
	('ue1','ue1','control.web.powderwireless.net');

insert into node_attributes set
	node_id="ue1", attrkey="sim_imsi", attrvalue="IMSI";
insert into node_attributes set
	node_id="ue1", attrkey="sim_sequence_number", attrvalue="1000000";


