<?php
#
# Copyright (c) 2000-2016 University of Utah and the Flux Group.
# 
# {{{EMULAB-LICENSE
# 
# This file is part of the Emulab network testbed software.
# 
# This file is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or (at
# your option) any later version.
# 
# This file is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public
# License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this file.  If not, see <http://www.gnu.org/licenses/>.
# 
# }}}
#

#
# Server side of creating a dataset.
#
function Do_CreateNews($idx = null)
{
    global $this_user;
    global $ajax_args;
    global $DBFieldErrstr, $PORTAL_GENESIS;

    if (!ISADMIN()) {
	SPITAJAX_ERROR(1, "Only admins please");
	return;
    }

    $this_idx   = $this_user->uid_idx();
    $this_uid   = $this_user->uid();
    # Allow for form precheck only. So JS code knows it will be fast. 
    $checkonly  = isset($ajax_args["checkonly"]) && $ajax_args["checkonly"];

    if (!isset($ajax_args["formfields"])) {
	SPITAJAX_ERROR(1, "Missing formfields");
	return;
    }
    $formfields = $ajax_args["formfields"];
    $errors  = array();

    $required = array("title", "body");
    foreach ($required as $field) {
	if (!isset($formfields[$field]) || $formfields[$field] == "") {
	    $errors[$field] = "Missing field";
	}
    }
    if (count($errors)) {
	SPITAJAX_ERROR(2, $errors);
	return;
    }
    if (!TBvalid_fulltext($formfields["title"])) {
	$errors["title"] = $DBFieldErrstr;
    }
    if (!TBvalid_html_fulltext($formfields["body"])) {
	$errors["body"] = $DBFieldErrstr;
    }
    if (count($errors)) {
	SPITAJAX_ERROR(2, $errors);
	return;
    }
    if ($checkonly) {
	SPITAJAX_RESPONSE(0);
	return;
    }
    $title = addslashes($formfields["title"]);
    $body  = addslashes($formfields["body"]);

    if ($idx) {
        $query_result =
            DBQueryWarn("update apt_news set ".
                        "  title='$title',body='$body' ".
                        "where idx='$idx'");
    }
    else {
        $query_result =
            DBQueryWarn("insert into apt_news set ".
                        "  title='$title',created=now(),author='$this_uid', ".
                        "  portals='$PORTAL_GENESIS', ".
                        "  author_idx='$this_idx',body='$body'");
    }
    if (!$query_result) {
	SPITAJAX_ERROR(-1, "Could not insert new news item");
	return;
    }
    SPITAJAX_RESPONSE("news.php");
}

#
# Server side of modifying a news item
#
function Do_ModifyNews()
{
    global $ajax_args;

    if (!isset($ajax_args["formfields"])) {
	SPITAJAX_ERROR(1, "Missing formfields");
	return;
    }
    $formfields = $ajax_args["formfields"];
    
    if (!isset($formfields["idx"])) {
	SPITAJAX_ERROR(1, "Missing news index");
	return;
    }
    if (!TBvalid_integer($formfields["idx"])) {
        SPITAJAX_ERROR(1, "Invalid news index");
        return;
    }
    return Do_CreateNews($formfields["idx"]);
}

function Do_DeleteNews()
{
    global $this_user;
    global $ajax_args;
    
    $this_idx = $this_user->uid_idx();
    $this_uid = $this_user->uid();

    if (!ISADMIN()) {
	SPITAJAX_ERROR(1, "Only admins please");
	return;
    }
    if (!isset($ajax_args["idx"])) {
	SPITAJAX_ERROR(1, "Missing news index");
	return;
    }
    if (!TBvalid_integer($ajax_args["idx"])) {
        SPITAJAX_ERROR(1, "Invalid news index");
        return;
    }
    $idx = $ajax_args["idx"];

    if (!DBQueryWarn("delete from apt_news where idx='$idx'")) {
	SPITAJAX_ERROR(-1, "Could not delete news item");
	return;
    }
    SPITAJAX_RESPONSE(0);
}

function Do_GetNews()
{
    global $this_user;
    global $ajax_args, $PORTAL_GENESIS;
    
    $idxclause= "";
    $count    = 6;

    if (isset($ajax_args["idx"]) && $ajax_args["idx"] != -1) {
        if (!TBvalid_integer($ajax_args["idx"])) {
            SPITAJAX_ERROR(1, "Invalid news index");
            return;
        }
        $idx = $ajax_args["idx"];
        $idxclause = "idx<=$idx and ";
    }
    if (isset($ajax_args["count"])) {
        if (!TBvalid_integer($ajax_args["count"])) {
            SPITAJAX_ERROR(1, "Invalid news count");
            return;
        }
        $count = $ajax_args["count"];
    }
    $query_result = DBQueryWarn("select * from apt_news ".
                                "where $idxclause ".
                                "      FIND_IN_SET('$PORTAL_GENESIS',portals) ".
                                "order by idx desc limit $count");
    if (!$query_result) {
        SPITAJAX_ERROR(1, "Could not get news items");
        return;
    }
    $news = array();
    while ($row = mysql_fetch_array($query_result)) {
        $blob = array("idx"    => $row["idx"],
                      "title"  => $row["title"],
                      "body"   => $row["body"],
                      "author" => $row["author"],
                      "created"=> DateStringGMT($row["created"]));
        $news[] = $blob;
    }
    SPITAJAX_RESPONSE($news);
}

# Local Variables:
# mode:php
# End:
?>
