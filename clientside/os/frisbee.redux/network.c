/*
 * Copyright (c) 2000-2018 University of Utah and the Flux Group.
 * 
 * {{{EMULAB-LICENSE
 * 
 * This file is part of the Emulab network testbed software.
 * 
 * This file is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 * 
 * This file is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this file.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * }}}
 */

/*
 * Network routines.
 */
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <net/if.h>
#include <ifaddrs.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <errno.h>
#include <assert.h>
#include <pthread.h>
#include "decls.h"
#include "utils.h"
#ifdef NO_SOCKET_TIMO
#include <sys/select.h>
#endif

#ifdef STATS
unsigned long nonetbufs;
#define DOSTAT(x)	(x)
#else
#define DOSTAT(x)
#endif

/* Max number of times to attempt bind to port before failing. */
#define MAXBINDATTEMPTS		1

#define IS_MCAST_ADDR(sa)	((ntohl((sa).s_addr) >> 28) == 14)

/* Max number of hops multicast hops. */
#define MCAST_TTL		5

uint32_t no_of_blocks_served = 0;
static int		sock = -1;
static int    inc_peer_req_sock = -1;
static int    tcp_sock = -1;
#ifdef USE_REUSEADDR_COMPAT
static int		selfsock = -1;
#endif
struct in_addr		myipaddr;
static int		nobufdelay = -1;
int			broadcast = 0;
int     nw_debug = 0;
static int		isclient = 0;
static int		sndportnum;	/* kept in network order */
/*
 * Convert a string to an IPv4 address.  We first try to interpret it as
 * an IPv4 address.  If that fails, we attempt to resolve it as a host name.
 * Return non-zero on success.
 */
int
GetIP(char *str, struct in_addr *in)
{
	struct hostent *he;

	if (inet_aton(str, in) == 0) {
		if ((he = gethostbyname(str)) == NULL)
			return 0;
		memcpy(in, he->h_addr, sizeof(*in));
	}

	return 1;
}

/*
 * Return the maximum size of a socket buffer.
 * Computes it dynamically on the first call.
 *
 * XXX assumes send/recv max sizes are the same.
 */
int
GetSockbufSize(void)
{

	static int sbsize = 0;

	if (sbsize == 0) {
		int sock;

		if ((sock = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0)
			FrisPfatal("Could not allocate a socket");

		for (sbsize = sockbufsize; sbsize > 0; sbsize -= (16*1024)) {
			int i = sbsize;
			if (setsockopt(sock, SOL_SOCKET, SO_SNDBUF,
				       &i, sizeof(i)) >= 0)
				break;
		}
		if (sbsize < 0) {
			int i = 0;
			unsigned int ilen = sizeof(i);
			if (getsockopt(sock, SOL_SOCKET, SO_SNDBUF,
				       &i, &ilen) < 0)
				i = sockbufsize;
			sbsize = i;
		}
		else {
			int i = 0;
			unsigned int ilen = sizeof(i);
			if (getsockopt(sock, SOL_SOCKET, SO_SNDBUF,
				       &i, &ilen) < 0)
				FrisPfatal("Could not read sockbuf size");
#ifdef linux
			/* In Linux, getsockopt returns 2 * actual value */
			if (i == 2 * sbsize)
				i = sbsize;
#endif
			if (i != sbsize) {
				FrisWarning("Actual socket buffer size is %d"
					    " (instead of %d)", i, sbsize);
				sbsize = i;
			}
		}
		close(sock);
		FrisLog("Maximum socket buffer size of %d bytes", sbsize);
	}
	return sbsize;
}

/*
 * Find the subnet broadcast address associated with the given interface
 * address. We use this to limit broadcasts to a single interface.
 * Returns zero if we successfully produced an address (in *bcaddr),
 * non-zero otherwise.
 */
static int
GetBcastAddr(struct in_addr *ifaddr, struct in_addr *bcaddr)
{
	struct ifaddrs *ifa, *nifa;
	struct sockaddr_in *sin;

	if (getifaddrs(&ifa) != 0) {
		FrisPwarning("Could not get interface list");
		return 1;
	}

	for (nifa = ifa; nifa != NULL; nifa = nifa->ifa_next) {
		if (nifa->ifa_addr->sa_family != AF_INET)
			continue;
		if ((nifa->ifa_flags & (IFF_UP|IFF_BROADCAST)) !=
		    (IFF_UP|IFF_BROADCAST))
			continue;
		sin = (struct sockaddr_in *)nifa->ifa_addr;
		if (ifaddr->s_addr == sin->sin_addr.s_addr) {
			*bcaddr = ((struct sockaddr_in *)nifa->ifa_broadaddr)->sin_addr;
			freeifaddrs(ifa);
			return 0;
		}
	}
	freeifaddrs(ifa);

	FrisWarning("Could not find interface %s", inet_ntoa(*ifaddr));
	return 1;
}

/*
 * Bind one port from the given range.
 * If lo == hi == 0, then we let the kernel choose.
 * If lo == hi != 0, then we must get that port.
 * Otherwise we loop over the range til we get one.
 * Returns the port bound or 0 if unsuccessful.
 */
static in_port_t
BindPort(int bind_sock, in_addr_t addr, in_port_t portlo, in_port_t porthi)
{
	struct sockaddr_in name;
	socklen_t sl = sizeof(name);

	name.sin_family      = AF_INET;
	name.sin_addr.s_addr = htonl(addr);
	name.sin_port        = htons(portlo);

	/*
	 * Let the kernel choose.
	 */
	if (portlo == 0) {
		if (bind(bind_sock, (struct sockaddr *)&name, sl) != 0) {
			FrisPwarning("Bind to %s:%d failed",
				     inet_ntoa(name.sin_addr), portlo);
			return 0;
		}

		if (getsockname(bind_sock, (struct sockaddr *)&name, &sl) < 0)
			FrisPfatal("could not determine bound port");
		return(ntohs(name.sin_port));
	}
	
	/*
	 * Specific port. Try a few times to get it.
	 */
	if (portlo == porthi) {
		int i = MAXBINDATTEMPTS;

		while (i) {
			if (bind(bind_sock, (struct sockaddr *)&name, sl) == 0)
				return portlo;

			if (--i) {
				FrisPwarning("Bind to %s:%d failed. "
					     "Will try %d more times!",
					     inet_ntoa(name.sin_addr), portlo, i);
				sleep(5);
			}
		}
		FrisPwarning("Bind to %s:%d failed",
			     inet_ntoa(name.sin_addr), portlo);
		return 0;
	}

	/*
	 * Port range, gotta loop through trying to grab one.
	 */
	while (portlo <= porthi) {
		name.sin_port = htons(portlo);
		if (bind(bind_sock, (struct sockaddr *)&name, sl) == 0)
			return portlo;
		portlo++;
	}

	return 0;
}

static void
Incoming_Peer_Req_SockInit (void)
{
  int peer_req_sockbufsize = INCOMING_PEER_REQ_SOCKBUFSIZE;
  int ret_portnum;
	if ((inc_peer_req_sock = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0)
  {
		FrisPfatal("Could not allocate a incoming peer request socket");
  }

	if (setsockopt(inc_peer_req_sock, SOL_SOCKET, SO_SNDBUF, &peer_req_sockbufsize, sizeof(peer_req_sockbufsize)) < 0)
  {
		FrisPwarning("Could not set inc peer req send socket buffer size to %d", peer_req_sockbufsize);
  }
	if (setsockopt(inc_peer_req_sock, SOL_SOCKET, SO_RCVBUF, &peer_req_sockbufsize, sizeof(peer_req_sockbufsize)) < 0)
  {
		FrisPwarning("Could not set inc peer req receive socket buffer size to %d", peer_req_sockbufsize);
  }

	in_addr_t addr = INADDR_ANY;

#ifdef USE_REUSEADDR
	/*
	 * For REUSEADDR to work in the face of unrelated apps that
	 * bind INADDR_ANY:port, we must NOT also bind INADDR_ANY.
	 */
	if (isclient && IS_MCAST_ADDR(mcastaddr))
		addr = ntohl(mcastaddr.s_addr);
#endif
	ret_portnum = BindPort(inc_peer_req_sock, addr, INC_PEER_REQ_PORT, INC_PEER_REQ_PORT);

	/*
	 * Could not get a port.
	 * Note that we exit with a magic value. This is for server
	 * wrapper-scripts so that they can differentiate this case
	 * and try again with a different port.
	 *
	 * Note also that if portlo == 0, it cannot be a port
	 * conflict so we do not retry.
	 */
	if (ret_portnum == 0) 
  {
		FrisError("Could not bind %s:%d!\n",
	      		  inet_ntoa(mcastaddr), ret_portnum);
		exit(INC_PEER_REQ_PORT ? EADDRINUSE : -1);
	}
  if (nw_debug > 0)
  {
  	FrisLog("Incoming Peer Request Socket Bound to %s:%d", inet_ntoa(mcastaddr), ret_portnum);
  }

#ifndef NO_SOCKET_TIMO
	/*
	 * We use a socket level timeout instead of polling for data.
	 */
	{
		struct timeval timeout;

		timeout.tv_sec  = 0;
		timeout.tv_usec = PKTRCV_TIMEOUT;
		if (setsockopt(inc_peer_req_sock, SOL_SOCKET, SO_RCVTIMEO, &timeout, sizeof(timeout)) < 0)
    {
			FrisPfatal("setsockopt(SOL_SOCKET, SO_RCVTIMEO)");
    }
	}
#endif
}

static void
CommonInit(int portlo, int porthi, int dobind)
{
  printf ("dobind %d\n", dobind);
	int			i;
	int 		opt = 1;
	char			buf[BUFSIZ];
	struct hostent		*he;
  struct sockaddr_in tcp_address;
  int timestampOn = 1;

	sockbufsize = GetSockbufSize();
	if ((sock = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0)
		FrisPfatal("Could not allocate a socket");

  printf ("Sockbufsize : %d\n", sockbufsize);
	i = sockbufsize;
	if (setsockopt(sock, SOL_SOCKET, SO_SNDBUF, &i, sizeof(i)) < 0)
		FrisPwarning("Could not set send socket buffer size to %d", i);
    
	i = sockbufsize;
	if (setsockopt(sock, SOL_SOCKET, SO_RCVBUF, &i, sizeof(i)) < 0)
		FrisPwarning("Could not set receive socket buffer size to %d",
			     i);
  timestampOn = 1;
  timestampOn = (1 << 4); 
  //timestampOn = timestampOn | SOF_TIMESTAMPING_RX_SOFTWARE;
  if (setsockopt(sock, SOL_SOCKET, SO_TIMESTAMPING, (int *) &timestampOn, sizeof(timestampOn)) < 0)
  {
    FrisPwarning ("SO_TIMESTAMP Failed");
  }


	if ((tcp_sock = socket(AF_INET , SOCK_STREAM , 0)) == 0)
    {   
      FrisPwarning ("TCP socket creation failed");
    }
  //set master socket to allow multiple connections ,  
  //this is just a good habit, it will work without this  
  if( setsockopt(tcp_sock, SOL_SOCKET, SO_REUSEADDR, (char *)&opt, sizeof(opt)) < 0 )   
    {
			FrisPwarning ("TCP socket setsockopt failed"); 
    }  
  //type of socket created  
  tcp_address.sin_family = AF_INET;   
  tcp_address.sin_addr.s_addr = INADDR_ANY;  
  if (nw_debug > 0)
  {
    FrisLog ("Binding TCP Server to port : %d", portlo); 
  }
  tcp_address.sin_port = htons(portlo);
  if (bind(tcp_sock, (struct sockaddr *)&tcp_address, sizeof(tcp_address))<0)
  {
    FrisPwarning ("TCP socket bind failed");
  }
  if (listen(tcp_sock, 64) < 0)
  {
    FrisPwarning ("TCP socket listen failed");
  }
	/*
	 * At present, we use a multicast address in both directions.
	 */
	if (IS_MCAST_ADDR(mcastaddr)) {
		unsigned int loop = 0, ttl = MCAST_TTL;
		struct ip_mreq mreq;

    if (nw_debug > 0)
    {
	  	FrisLog("Using Multicast %s", inet_ntoa(mcastaddr));
    }

		mreq.imr_multiaddr.s_addr = mcastaddr.s_addr;

		if (mcastif.s_addr)
			mreq.imr_interface.s_addr = mcastif.s_addr;
		else
			mreq.imr_interface.s_addr = htonl(INADDR_ANY);

		if (setsockopt(sock, IPPROTO_IP, IP_ADD_MEMBERSHIP,
			       &mreq, sizeof(mreq)) < 0)
			FrisPfatal("setsockopt(IPPROTO_IP, IP_ADD_MEMBERSHIP)");

		if (setsockopt(sock, IPPROTO_IP, IP_MULTICAST_TTL,
			       &ttl, sizeof(ttl)) < 0) 
			FrisPfatal("setsockopt(IPPROTO_IP, IP_MULTICAST_TTL)");

		/* Disable local echo */
		if (setsockopt(sock, IPPROTO_IP, IP_MULTICAST_LOOP,
			       &loop, sizeof(loop)) < 0)
			FrisPfatal("setsockopt(IPPROTO_IP, IP_MULTICAST_LOOP)");

		if (mcastif.s_addr &&
		    setsockopt(sock, IPPROTO_IP, IP_MULTICAST_IF,
			       &mcastif, sizeof(mcastif)) < 0) {
			FrisPfatal("setsockopt(IPPROTO_IP, IP_MULTICAST_IF)");
		}

#ifdef USE_REUSEADDR
		/*
		 * Allow use of the desired port in the presense of other
		 * non-MC use. Also allows for multiple clients of the same
		 * stream.
		 */
		if (isclient) {
			i = 1;
			if (setsockopt(sock, SOL_SOCKET, SO_REUSEADDR,
				       &i, sizeof(i)))
				FrisWarning("Could not set SO_REUSEADDR");
		}
#endif

#ifdef WITH_IGMP
		IGMPInit(&mcastif, &mcastaddr);
#endif
	}
	else if (broadcast) {
		FrisLog("Setting broadcast mode");

		/*
		 * If they are using the local broadcast address and they
		 * have specified an interface, attempt to limit broadcasts
		 * to that interface by using the subnet broadcast address.
		 * Otherwise we issue a dire warning about the consequences
		 * of broadcasting to all interfaces.
		 */
		if (ntohl(mcastaddr.s_addr) == INADDR_BROADCAST) {
			struct in_addr bcaddr;
			if (mcastif.s_addr &&
			    GetBcastAddr(&mcastif, &bcaddr) == 0) {
				FrisLog("Limiting broadcasts using %s",
					inet_ntoa(bcaddr));
				mcastaddr = bcaddr;
			} else
				FrisWarning("WARNING: will broadcast "
					    "to ALL configured interfaces!");
		}

		i = 1;
		if (setsockopt(sock, SOL_SOCKET, SO_BROADCAST,
			       &i, sizeof(i)) < 0)
			FrisPfatal("setsockopt(SOL_SOCKET, SO_BROADCAST)");
	}

	if (dobind) {
		in_addr_t addr = INADDR_ANY;

#ifdef USE_REUSEADDR
		/*
		 * For REUSEADDR to work in the face of unrelated apps that
		 * bind INADDR_ANY:port, we must NOT also bind INADDR_ANY.
		 */
		if (isclient && IS_MCAST_ADDR(mcastaddr))
			addr = ntohl(mcastaddr.s_addr);
#endif
		portnum = BindPort(sock, addr, portlo, porthi);

		/*
		 * Could not get a port.
		 * Note that we exit with a magic value. This is for server
		 * wrapper-scripts so that they can differentiate this case
		 * and try again with a different port.
		 *
		 * Note also that if portlo == 0, it cannot be a port
		 * conflict so we do not retry.
		 */
		if (portnum == 0) {
			FrisError("Could not bind %s:%d!\n",
				  inet_ntoa(mcastaddr), portnum);
			exit(portlo ? EADDRINUSE : -1);
		}
		FrisLog("Bound to %s:%d", inet_ntoa(mcastaddr), portnum);
	} else {
		portnum = portlo;
		FrisLog("NOT binding to %s:%d", inet_ntoa(mcastaddr), portnum);
	}
	sndportnum = htons(portnum);

#ifndef NO_SOCKET_TIMO
	/*
	 * We use a socket level timeout instead of polling for data.
	 */
	{
		struct timeval timeout;

		timeout.tv_sec  = 0;
		timeout.tv_usec = PKTRCV_TIMEOUT;
		if (setsockopt(sock, SOL_SOCKET, SO_RCVTIMEO,
			       &timeout, sizeof(timeout)) < 0)
			FrisPfatal("setsockopt(SOL_SOCKET, SO_RCVTIMEO)");
	}
#endif

	/*
	 * If a specific interface IP is specified, use that to
	 * tag our outgoing packets.  Otherwise we use the IP address
	 * associated with our hostname.
	 */
	if (mcastif.s_addr)
		myipaddr.s_addr = mcastif.s_addr;
	else {
		if (gethostname(buf, sizeof(buf)) < 0)
			FrisPfatal("gethostname failed");

		if ((he = gethostbyname(buf)) == 0)
			FrisFatal("gethostbyname: %s", hstrerror(h_errno));

		memcpy((char *)&myipaddr, he->h_addr, sizeof(myipaddr));
	}

	/*
	 * Compute the out of buffer space delay.
	 */
	if (nobufdelay < 0)
		nobufdelay = sleeptime(100, NULL, 1);
}

int
ClientNetInit(int port)
{
	isclient = 1;
#ifdef SAME_HOST_HACK
	CommonInit(port, port, 0);
#else
	CommonInit(port, port, 1);
#endif
  Incoming_Peer_Req_SockInit ();

#ifdef USE_REUSEADDR_COMPAT
	/*
	 * Bind a unicast socket for our interface address and the port.
	 *
	 * XXX this is for backward compatibility with an older server and
	 * is used for two purposes.
	 *
	 * One, is so that we can receive a unicast JOIN reply from an old
	 * server (the new server always multicasts JOIN replies). The old
	 * client would see this reply because it would just bind the port
	 * using INADDR_ANY in CommonInit, insuring that it would get the
	 * JOIN reply. But with SO_REUSEADDR, we do not bind to INADDR_ANY,
	 * we bind explicitly to the MC address and port and thus would not
	 * see the reply without also binding the unicast (interface) address
	 * and port.
	 *
	 * Two, we send all our multicast packets (joins and block requests)
	 * out this interface so that the packets are stamped with the IP of
	 * the interface and not the MC IP. The old server required that the
	 * packet source IP match the source IP in the frisbee packet header
	 * (the new server allows packets that come from the MC address).
	 * We could just set the frisbee packet header srcip to be the MC
	 * address instead, but we use that address for logging and stats
	 * and want to keep it correct.
	 */
	if (myipaddr.s_addr && IS_MCAST_ADDR(mcastaddr)) {
		struct sockaddr_in name;
		int i;

		if ((selfsock = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0)
			FrisPfatal("Could not allocate a socket");

		i = MCAST_TTL;
		if (setsockopt(selfsock, IPPROTO_IP, IP_MULTICAST_TTL,
			       &i, sizeof(i)) < 0) 
			FrisWarning("Could not set MC TTL");

		/* Disable local echo */
		i = 0;
		if (setsockopt(selfsock, IPPROTO_IP, IP_MULTICAST_LOOP,
			       &i, sizeof(i)) < 0)
			FrisWarning("Could not clear local echo");

		/* Make sure we use the correct interface */
		if (mcastif.s_addr &&
		    setsockopt(selfsock, IPPROTO_IP, IP_MULTICAST_IF,
			       &mcastif, sizeof(mcastif)) < 0) {
			FrisWarning("Could not set MCAST_IF");
		}

		/* Set REUSEADDR */
		i = 1;
		if (setsockopt(selfsock, SOL_SOCKET, SO_REUSEADDR,
			       &i, sizeof(i)))
			FrisWarning("Could not set SO_REUSEADDR");

		name.sin_family = AF_INET;
		name.sin_port = htons(portnum);
		name.sin_addr.s_addr = myipaddr.s_addr;
		if (bind(selfsock, (struct sockaddr *)&name, sizeof(name)) < 0)
			FrisPfatal("Could not bind to %s:%d",
				   inet_ntoa(name.sin_addr), portnum);

#ifndef NO_SOCKET_TIMO
		/*
		 * We use a socket level timeout instead of polling for data.
		 */
		{
			struct timeval timeout;

			timeout.tv_sec  = 0;
			timeout.tv_usec = PKTRCV_TIMEOUT;
			if (setsockopt(selfsock, SOL_SOCKET, SO_RCVTIMEO,
				       &timeout, sizeof(timeout)) < 0)
				FrisPfatal("setsockopt(SOL_SOCKET, SO_RCVTIMEO)");
		}
#endif
	}
#endif

	return 1;
}

unsigned long
ClientNetID(void)
{
	return ntohl(myipaddr.s_addr);
}

int
ServerNetInit(int portlo, int porthi)
{
	isclient = 0;
	CommonInit(portlo, porthi, 1);

#ifdef linux
	/*
	 * Enabled extended error reporting so that we get back ENOBUFS
	 * when we overrun the sent socket or NIC send buffers.
	 * For now we just do this on the client.
	 */
	{
		int i = 1;
		if (setsockopt(sock, SOL_IP, IP_RECVERR, &i, sizeof(i)) < 0)
			FrisPwarning("Could not enable extended errors");
	}
#endif

	return 1;
}

/*
 * XXX hack.
 *
 * Cisco switches without a multicast router defined have an unfortunate
 * habit of losing our IGMP membership.  This function allows us to send
 * a report message to remind the switch we are still around.
 *
 * We need a better way to do this!
 */
int
NetMCKeepAlive(void)
{
	struct ip_mreq mreq;

	if (broadcast || (ntohl(mcastaddr.s_addr) >> 28) != 14)
		return 0;

	if (sock == -1)
		return 1;

#ifdef WITH_IGMP
	/* Send a direct V2 report packet if possible */
	if (IGMPSendReport() == 0)
		return 0;
#endif

	mreq.imr_multiaddr.s_addr = mcastaddr.s_addr;
	if (mcastif.s_addr)
		mreq.imr_interface.s_addr = mcastif.s_addr;
	else
		mreq.imr_interface.s_addr = htonl(INADDR_ANY);

	if (setsockopt(sock, IPPROTO_IP, IP_DROP_MEMBERSHIP,
		       &mreq, sizeof(mreq)) < 0 ||
	    setsockopt(sock, IPPROTO_IP, IP_ADD_MEMBERSHIP,
		       &mreq, sizeof(mreq)) < 0)
		return 1;

	return 0;
}

/*
 * Look for a packet on the socket. Propogate the errors back to the caller
 * exactly as the system call does. Remember that we set up a socket timeout
 * above, so we will get EWOULDBLOCK errors when no data is available. 
 *
 * The amount of data received is determined from the datalen of the hdr.
 * All packets are actually the same size/structure. 
 *
 * Returns 0 for a good packet, 1 for a bad packet, -1 on timeout.
 */
int
PacketReceive(Packet_t *p)
{
	struct sockaddr_in from;
	int		   mlen;
	unsigned int	   alen;

#ifdef NO_SOCKET_TIMO
	fd_set		ready;
	struct timeval	tv;
	int		rv;

	tv.tv_sec = 0;
	tv.tv_usec = PKTRCV_TIMEOUT;
	FD_ZERO(&ready);
	FD_SET(sock, &ready);
	rv = select(sock+1, &ready, NULL, NULL, &tv);
	if (rv < 0) {
		if (errno == EINTR)
			return -1;
		FrisPfatal("PacketReceive(select)");
	}
	if (rv == 0)
		return -1;
#endif
	alen = sizeof(from);
	bzero(&from, alen);
	if ((mlen = recvfrom(sock, p, sizeof(*p), 0,
			     (struct sockaddr *)&from, &alen)) < 0) {
		if (errno == EWOULDBLOCK || errno == EINTR)
			return -1;
		FrisPfatal("PacketReceive(recvfrom)");
	}

	/*
	 * Basic integrity checks
	 */
	if ((uint32_t)mlen < sizeof(p->hdr) + p->hdr.datalen) {
		FrisLog("Bad message length (%d != %d)",
			mlen, p->hdr.datalen);
		return 1;
	}
#ifdef SAME_HOST_HACK
	/*
	 * If using a host alias for the client, a message may get
	 * the wrong IP, so rig the IP check to make it always work.
	 */
	if (p->hdr.srcip != from.sin_addr.s_addr)
		from.sin_addr.s_addr = p->hdr.srcip;

	/*
	 * Also, we aren't binding to a port on the client side, so the
	 * first message to the server will contain the actual port we
	 * will use from now on.
	 */
	if (!isclient && sndportnum == htons(portnum) &&
	    sndportnum != from.sin_port)
		sndportnum = from.sin_port;
#endif
	/*
	 * XXX accept packets from the MC address. This will be the case with
	 * newer clients that bind to the MC address instead of INADDR_ANY.
	 *
	 * Note that on a client, certain packets should only come from the
	 * server. These include: BLOCK replies and PROGRESS requests.
	 * Don't rewrite the address in these cases so that the following
	 * check will catch them (or a later caller check on hdr.srcip).
	 */
	if (from.sin_addr.s_addr == mcastaddr.s_addr) {
		if (isclient &&
		    (p->hdr.subtype == PKTSUBTYPE_BLOCK ||
		     (p->hdr.subtype == PKTSUBTYPE_PROGRESS &&
		      p->hdr.type == PKTTYPE_REQUEST)))
			;
		else
			from.sin_addr.s_addr = p->hdr.srcip;
	}

  
  /*SWP2P - Temp - Commenting the below section to solve the below problem*/
  /*Packet's source is changed to switch IP when the packet is sent out.*/
#if 0 /*SWP2P - Temp - Start*/
	if (p->hdr.srcip != from.sin_addr.s_addr) {
		FrisLog("Bad message source (%x != %x)",
			ntohl(from.sin_addr.s_addr), ntohl(p->hdr.srcip));
		return 1;
	}

	if (sndportnum != from.sin_port) {
		FrisLog("Bad message port (%d != %d)",
			ntohs(from.sin_port), ntohs(sndportnum));
		return 1;
	}
#endif /*SWP2P - Temp - End*/
	return 0;
}

Swp2pPeerStats_t *
peer_stat_init (Swp2pPeerStats_t *peer)
{
  int SendQDelay = -1;
	int sockbufsize = SOCKBUFSIZE;
  int i = 0;
  struct sockaddr_in serv_addr;
  int port = 54321;
  char *ipaddr_str;
  /*Ininitalize the peer Send Queue and lock*/

  pthread_mutex_init(&(peer->SendQLock), NULL);
  queue_init(&(peer->SendQ));

  /*Initialize the cond_wait time delay*/
  SendQDelay = sleeptime(1, NULL, 1);

#ifdef CONDVARS_WORK
  pthread_cond_init(&(peer->SendQCond), NULL);
  peer->SendQTimespec.tv_sec = SendQDelay / 1000000;
  peer->SendQTimespec.tv_nsec = (SendQDelay % 1000000) * 1000;
#endif

  peer->stop_worker = 0;

  /*Start the peer's send thread*/

  if ((peer->psock = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    {
      FrisPfatal("Could not allocate a tcp socket for peer");
    }

  if (nw_debug > 0)
  {
    FrisLog ("Peer : %s Creating New Sock", inet_ntoa (peer->peer_ipaddr));
  }
  ipaddr_str = inet_ntoa (peer->peer_ipaddr);
  if(inet_pton(AF_INET, ipaddr_str, &serv_addr.sin_addr)<=0)
  {
    FrisLog ("Invalid IP Address");
  }
//  serv_addr.sin_addr.s_addr = htonl(peer->peer_ipaddr.s_addr); 
  serv_addr.sin_family = AF_INET;
  serv_addr.sin_port = htons(port);
  if (connect(peer->psock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)
  {
    FrisLog ("Peer Connection Failed");
  }

  if (nw_debug > 0)
  {
    FrisLog ("Peer : %s Send Sock : %d", inet_ntoa (peer->peer_ipaddr), peer->psock);
  }
  return peer;
}

void
peer_stat_deinit_all ()
{
  int i = 0;
	for (i = 0; i < MAX_PEER_CLIENTS; i++)
  {
    if (PeerList[i].peer_ipaddr.s_addr > 0)
    {
      peer_stat_deinit (PeerList[i].peer_ipaddr);
    }
  }
}

void
peer_stat_deinit (struct in_addr ipaddr)
{
  int i = 0;
  Swp2pPeerStats_t *peer = NULL;
  SQelem_t *sqel = NULL;

  if (nw_debug > 0)
  {
    FrisLog ("Peer Stat Deinit for %s", inet_ntoa(ipaddr));
  }
	for (i = 0; i < MAX_PEER_CLIENTS; i++)
  {
    if (PeerList[i].peer_ipaddr.s_addr == ipaddr.s_addr)
    {
      peer = &PeerList[i];
      break;
    }
  }
  if (i == MAX_PEER_CLIENTS)
  {
    /*No peer found to clear*/
    return;
  }
  peer->peer_ipaddr.s_addr = 0;
  
  /*Stop the PeerWorker Thread*/
  if (peer->send_tid > 0)
  {
    peer->stop_worker = 1;
    pthread_cancel (peer->send_tid);
    pthread_join (peer->send_tid, NULL);
    peer->send_tid = 0;
    peer->stop_worker = 0;
  }

  /*Clear the peer sock id*/
  if (peer->psock > 0)
  {
    close (peer->psock);
    peer->psock = 0;
  }

  if (peer->peer_recv_sock > 0)
  {
    close (peer->peer_recv_sock);
    peer->peer_recv_sock = 0;
  }

  /*Destroy the mutex*/
  pthread_mutex_destroy (&(peer->SendQLock));

  /*Destroy the condition variable*/
  pthread_cond_destroy (&(peer->SendQCond));

  /*Clear the Queue*/
  while (1)
  {
    if (queue_empty (&(peer->SendQ)))
    {
      break;
    }
    sqel = (SQelem_t *) queue_first (&(peer->SendQ));
    queue_remove(&(peer->SendQ), sqel, SQelem_t *, chain);
    free (sqel);
  }

  peer->sent_count = 0;
  peer->sending_time_spent.tv_sec = 0;
  peer->sending_time_spent.tv_usec = 0;
  peer->req_time_spent_on_queue.tv_sec = 0;
  peer->req_time_spent_on_queue.tv_usec = 0;
  peer->served_pkts_delay.tv_sec = 0;
  peer->served_pkts_delay.tv_usec = 0;
  peer->tcp_pkt_dly.tv_sec = 0;
  peer->tcp_pkt_dly.tv_nsec = 0;
  peer->served_pkts = 0;
  memset (&(peer->tcp_buf), 0, 1048);
  peer->tcp_buf_dlen = 0;

  return;
}

Swp2pPeerStats_t *
get_peer_stat_struct (struct in_addr ipaddr)
{
  int i = 0;
  int first_unallocated = -1;

	for (i = 0; i < MAX_PEER_CLIENTS; i++)
  {
    if (PeerList[i].peer_ipaddr.s_addr == ipaddr.s_addr)
    {
      //printf ("Found PeerList at index %d for ip_addr = %s\n", i, inet_ntoa (ipaddr));
      return &PeerList[i];
    }
    else if ((first_unallocated == -1) && (PeerList[i].peer_ipaddr.s_addr == 0))
    {
      first_unallocated = i;
    }
  }

  /*Give the first unallocated slot to this peer*/
  if (first_unallocated >= MAX_PEER_CLIENTS)
  {
    return NULL;
  }
  //printf ("Allocating a new entry in PeerList at index %d for ip_addr = %s\n", first_unallocated, inet_ntoa (ipaddr));

  PeerList[first_unallocated].peer_ipaddr.s_addr = ipaddr.s_addr;

  /*Initialize the SendQ for this peer*/
  peer_stat_init (&PeerList[first_unallocated]);

  return &PeerList[first_unallocated];
}

int
Swp2p_Peer_Request_Receive(Packet_t *p)
{
	struct sockaddr_in from;
	int		   mlen;
	unsigned int	   alen;
#ifdef NO_SOCKET_TIMO
	fd_set		ready;
	struct timeval	tv;
	int		rv;

  /*Set the timeout for receive*/
	tv.tv_sec = 0;
	tv.tv_usec = PKTRCV_TIMEOUT;

	FD_ZERO(&ready);
	FD_SET(inc_peer_req_sock, &ready);

	rv = select(inc_peer_req_sock+1, &ready, NULL, NULL, &tv);
	if (rv < 0) 
  {
		if (errno == EINTR)
    {
			return -1;
    }
		FrisPfatal("Swp2p_Peer_Request_Receive(select)");
	}
	if (rv == 0)
  {
		return -1;
  }
#endif

	alen = sizeof(from);
	bzero(&from, alen);

	if ((mlen = recvfrom(inc_peer_req_sock, p, sizeof(*p),
                       0, (struct sockaddr *)&from, &alen)) < 0)
  {
		if (errno == EWOULDBLOCK || errno == EINTR)
    {
			return -1;
    }
		FrisPfatal("Swp2p_Peer_Request_Receive(recvfrom)");
	}

	/*
	 * Basic integrity checks
	 */
	if ((uint32_t)mlen < sizeof(p->hdr) + p->hdr.datalen) 
  {
		FrisLog("Swp2p_Peer_Request_Receive : Bad message length (%d != %d)", mlen, p->hdr.datalen);
		return 1;
	}
	return 0;
}

Packet_t *
tcp_udp_PacketReceive(Swp2pPeerStats_t *PeerList, int *retval, int *no_of_pkts, struct timespec *pkt_recv_time, Packet_t *p_big)
{
  int tcp_read_size = 0;
	struct sockaddr_in from;
	int		   mlen;
	unsigned int	   alen;
  int max_sd, sd, activity, new_socket;
  fd_set readfds;
  int i = 0;
  Swp2pPeerStats_t *peer = NULL;
	struct timeval	tv;
	tv.tv_sec = 0;
	tv.tv_usec = PKTRCV_TIMEOUT;
  int remaining_len = 0, already_copied = 0;
  char t_buf[108000];
  char c_buf[1080]; 
  FD_ZERO(&readfds);
  FD_SET(tcp_sock, &readfds);
  max_sd = tcp_sock;
  Packet_t *p = NULL;
  int p_big_count = 0;
  int p_big_max = MAXBLOCKSINRECV;
  int max_clients = MAXPARALLELCLIENTS;
  struct timespec tcp_pkt_recv_time;

  if (p_big == NULL)
  {
    FrisLog ("Memory allocation failed");
    *retval = 1;
    return NULL;
  }
  for (i = 0; i < MAX_PEER_CLIENTS; i++)
  {
    sd = PeerList[i].peer_recv_sock;
    if (sd > 0)
    {
      FD_SET( sd , &readfds);
    }
    if(sd > max_sd)
    {
      max_sd = sd;
    }
  }
  /*Add the UDP socket too*/
  FD_SET(sock, &readfds);
  if (sock > max_sd)
  {
    max_sd = sock;
  }
  activity = select(max_sd+1 , &readfds , NULL , NULL , &tv);

  if (activity == 0)
  {
    *retval = -1;
    return p_big; 
  }
  if ((activity < 0) && (errno!=EINTR))
  {
    FrisLog ("Select Error");
  }
  if (FD_ISSET (tcp_sock, &readfds))
  {
    if (nw_debug > 0)
    {
      FrisLog ("Activity on tcp_sock");
    }
    /*New Connection*/
	  alen = sizeof(from);
    if ((new_socket = accept(tcp_sock, (struct sockaddr *)&from, (socklen_t*)&alen))<0)
    {
      FrisLog ("Accept failed");
    }
    if (nw_debug > 0)
    {
      FrisLog ("New connection, socket fd is %d , ip is : %s , port : %d \n",
            new_socket , inet_ntoa(from.sin_addr) , ntohs (from.sin_port));
    }

    peer = get_peer_stat_struct (from.sin_addr);
    peer->peer_recv_sock = new_socket;
    int timestampOn = 1;
    timestampOn = (1 << 4); 
    //timestampOn = timestampOn | SOF_TIMESTAMPING_RX_SOFTWARE;
    if (setsockopt(peer->peer_recv_sock, SOL_SOCKET, SO_TIMESTAMPING, (int *) &timestampOn, sizeof(timestampOn)) < 0)
    {
      FrisPwarning ("SO_TIMESTAMP Failed");
    }
    *retval = 1;
    return p_big;
  }
  if (FD_ISSET (sock, &readfds))
  {
    /*Data ready in UDP Socket*/
    /*Time Stamping version - Start*/
    struct msghdr   msg;
    struct iovec    iov;
    char            ctrl[CMSG_SPACE(sizeof(struct timespec))];
    struct cmsghdr *cmsg = (struct cmsghdr *) &ctrl;

    msg.msg_control      = (char *) ctrl;
    msg.msg_controllen   = sizeof(ctrl);

    msg.msg_name         = NULL;
    msg.msg_namelen      = 0;
    msg.msg_iov          = &iov;
    msg.msg_iovlen       = 1;
    iov.iov_base         = p_big;
    iov.iov_len          = sizeof(Packet_t);

    mlen = recvmsg(sock, &msg, 0);
    if (mlen < 0)
    {
      *retval = 1;
      return p_big;
    }
    else
    {
      struct msghdr *msgp = &msg;
      struct cmsghdr  *cmsg;
      for (cmsg = CMSG_FIRSTHDR(msgp); cmsg != NULL; cmsg = CMSG_NXTHDR(msgp, cmsg))
      {
        if ((cmsg->cmsg_level == SOL_SOCKET)&&(cmsg->cmsg_type == SO_TIMESTAMPING))
        {
          memcpy(pkt_recv_time, CMSG_DATA(cmsg), sizeof (struct timespec));
        }
      }
      p_big_count ++;
    }
    /*Time Stamping version - end*/
#if 0
    /*No Time Stamping version - start*/
  	alen = sizeof(from);
  	bzero(&from, alen);
	  if ((mlen = recvfrom(sock, p_big, sizeof(Packet_t), 0, (struct sockaddr *)&from, &alen)) < 0) 
    {
		  if (errno == EWOULDBLOCK || errno == EINTR)
      {
			  *retval = 1;
        return p_big;
      }
  		FrisPfatal("PacketReceive(recvfrom)");
	  }
    p_big_count ++;
    /*No Time Stamping version - end*/
#endif
  }

  for (i = 0; i < MAX_PEER_CLIENTS; i++)
  {
    sd = PeerList[i].peer_recv_sock;
    if (sd == 0)
      continue;

    if (FD_ISSET(sd , &readfds))
    {
      memset (t_buf, 0, 108000);
      memset (c_buf, 0, 1080);
      tcp_read_size = (p_big_max-1) * 1048;
      /*Data from one of the peers*/
      //FrisLog ("Sizeof *p = %d", sizeof (*p));
#if 0
      /*Time Stamping version - Start*/
      struct msghdr   msg;
      struct iovec    iov;
      char            ctrl[CMSG_SPACE(sizeof(struct timespec))];
      struct cmsghdr *cmsg = (struct cmsghdr *) &ctrl;

      msg.msg_control      = (char *) ctrl;
      msg.msg_controllen   = sizeof(ctrl);

      msg.msg_name         = NULL;
      msg.msg_namelen      = 0;
      msg.msg_iov          = &iov;
      msg.msg_iovlen       = 1;
      iov.iov_base         = t_buf;
      iov.iov_len          = tcp_read_size;

      mlen = recvmsg(sd, &msg, 0);
      if (mlen <= 0)
      {
        getpeername(sd , (struct sockaddr*)&from, (socklen_t*)&alen);
        printf("Peer disconnected , ip %s , port %d \n", inet_ntoa(from.sin_addr) , ntohs(from.sin_port));
        close (sd);
        PeerList[i].peer_recv_sock = 0;
      }
      else
      {
        struct msghdr *msgp = &msg;
        struct cmsghdr  *cmsg;
        struct timespec cur_time, temp_result;
        for (cmsg = CMSG_FIRSTHDR(msgp); cmsg != NULL; cmsg = CMSG_NXTHDR(msgp, cmsg))
        {
          if ((cmsg->cmsg_level == SOL_SOCKET)&&(cmsg->cmsg_type == SO_TIMESTAMPING))
          {
            memcpy(&tcp_pkt_recv_time, CMSG_DATA(cmsg), sizeof (struct timespec));
          }
        }
        clock_gettime (CLOCK_REALTIME,  &cur_time);
  			timespec_diff (&pkt_recv_time, &cur_time, &temp_result);
  		  PeerList[i].tcp_pkt_dly = timespec_add(temp_result, PeerList[i].tcp_pkt_dly);
      }
      /*Time Stamping version - end*/
#endif
      if ((mlen = read(sd, t_buf, tcp_read_size)) == 0)
      {
        getpeername(sd , (struct sockaddr*)&from, (socklen_t*)&alen);
        printf("Peer disconnected , ip %s , port %d \n", inet_ntoa(from.sin_addr) , ntohs(from.sin_port));
        close (sd);
        PeerList[i].peer_recv_sock = 0;
      }
      //FrisLog ("mlen = %d", mlen);
      /*Check if buffer is already having spilled over stuff*/
      if (PeerList[i].tcp_buf_dlen != 0)
      {
        /*We already have left over data*/
        /*Copy the first few bytes from tcp_buf*/
        //FrisLog ("Copied %d from tcp_buf to c_buf", PeerList[i].tcp_buf_dlen);

        remaining_len = 1048 - PeerList[i].tcp_buf_dlen;
        if (mlen < remaining_len)
        {
          //FrisLog ("mlen %d  < remaining_len %d\n", mlen, remaining_len);
          memcpy (&(PeerList[i].tcp_buf[PeerList[i].tcp_buf_dlen]), t_buf, mlen);
          PeerList[i].tcp_buf_dlen += mlen;
          *retval = 1;
          return p_big;
        }
        /*Copy first half of msg from peer buf*/
        memcpy (c_buf, &(PeerList[i].tcp_buf), PeerList[i].tcp_buf_dlen);
        /*Copy second half of msg from received msg*/
        memcpy (c_buf+PeerList[i].tcp_buf_dlen, t_buf, remaining_len);

        //FrisLog ("Copied %d from new data t_buf to c_buf", remaining_len);
        //FrisLog ("Sum of copied = %d", PeerList[i].tcp_buf_dlen + remaining_len);

        PeerList[i].tcp_buf_dlen = 0;
        memset (&(PeerList[i].tcp_buf), 0, 1048);
        
        /*Copy it to p_big which will be returned*/
        memcpy (&p_big[p_big_count], c_buf, 1048);
        p_big_count ++;
        //FrisLog ("p_big_count++ at 870 : %d", p_big_count);
        /*Copy the remaining data we just got to buf*/
        already_copied = remaining_len;
        remaining_len = mlen - already_copied;

        while (remaining_len >= 1048)
        {
          memcpy (&p_big[p_big_count], t_buf+already_copied, 1048);
          already_copied += 1048;
          remaining_len -= 1048;
          p_big_count ++;
        }
        //FrisLog ("p_big_count at 882 = %d mlen = %d remaining_len : %d already_copied : %d", p_big_count, mlen, remaining_len, already_copied);
        //FrisLog ("Remaining data len %d", remaining_len);
        if (remaining_len > 0)
         {
            /*Copy this into buffer*/
            PeerList[i].tcp_buf_dlen = remaining_len;
            memcpy (&(PeerList[i].tcp_buf), t_buf+already_copied, remaining_len);
         }
        //FrisLog ("tcp_buf_dlen %d", PeerList[i].tcp_buf_dlen);
        /*Add the incoming data and make the packet whole*/
        if (p_big_count == 1)
        {
          mlen = 1048;
        }
      }
      else if (mlen < 1048)
      {
        //FrisLog ("Copying smaller %d data to tcp_buf", mlen);
        PeerList[i].tcp_buf_dlen = mlen;
        memcpy (&(PeerList[i].tcp_buf), t_buf, mlen);
        *retval = 1;
        return p_big;
      }
      else if (mlen == 1048)
      {
        memcpy (&p_big[p_big_count], t_buf, 1048);
        p_big_count ++;
      }
      else if (mlen > 1048)
      {
        memcpy (&p_big[p_big_count], t_buf, 1048);
        p_big_count ++;
        //FrisLog ("p_big_count++ at 916 : %d", p_big_count);

        /*Copy the remaining data we just got to buf*/
        already_copied = 1048;
        remaining_len = mlen - already_copied;

        while (remaining_len >= 1048)
        {
          memcpy (&p_big[p_big_count], t_buf+already_copied, 1048);
          already_copied += 1048;
          remaining_len -= 1048;
          p_big_count ++;
        }
        //FrisLog ("p_big_count at 929 = %d mlen = %d remaining_len : %d already_copied : %d", p_big_count, mlen, remaining_len, already_copied);
        //FrisLog ("Remaining data len %d", remaining_len);
        if (remaining_len > 0)
         {
            /*Copy this into buffer*/
            PeerList[i].tcp_buf_dlen = remaining_len;
            memcpy (&(PeerList[i].tcp_buf), t_buf+already_copied, remaining_len);
         }
        //FrisLog ("tcp_buf_dlen %d", PeerList[i].tcp_buf_dlen);
        /*Add the incoming data and make the packet whole*/
        if (p_big_count == 1)
        {
          mlen = 1048;
        }
      }
    }
  }

#if 0
  FrisLog ("Number of received messages = %d", p_big_count);
  for (i = 0; i < p_big_count; i++)
  {
    p = &p_big[i];
    FrisLog ("p->hdr.type    = %d", p->hdr.type);
    FrisLog ("p->hdr.datalen = %d", p->hdr.datalen);
  }
#endif
#if 0
	/*
	 * Basic integrity checks
	 */
	if ((uint32_t)mlen < sizeof(p->hdr) + p->hdr.datalen) 
  {
		FrisLog("Bad message length (%d != %d)",
			mlen, p->hdr.datalen);
		return 1;
	}
#ifdef SAME_HOST_HACK
	/*
	 * If using a host alias for the client, a message may get
	 * the wrong IP, so rig the IP check to make it always work.
	 */
	if (p->hdr.srcip != from.sin_addr.s_addr)
		from.sin_addr.s_addr = p->hdr.srcip;

	/*
	 * Also, we aren't binding to a port on the client side, so the
	 * first message to the server will contain the actual port we
	 * will use from now on.
	 */
	if (!isclient && sndportnum == htons(portnum) &&
	    sndportnum != from.sin_port)
		sndportnum = from.sin_port;
#endif
#endif
  *retval = 0;
  *no_of_pkts = p_big_count;
	return p_big;
}


#ifdef USE_REUSEADDR_COMPAT
/*
 * Same as PacketReceive but read from unicast (self) socket.
 *
 * Returns 0 for a good packet, 1 for a bad packet, -1 on timeout.
 */
int
PacketRequest(Packet_t *p)
{
	struct sockaddr_in from;
	int		   mlen;
	unsigned int	   alen;

	if (selfsock < 0)
		return -1;

#ifdef NO_SOCKET_TIMO
	{
		fd_set		ready;
		struct timeval	tv;
		int		rv, maxfd;

		tv.tv_sec = 0;
		tv.tv_usec = PKTRCV_TIMEOUT;
		FD_ZERO(&ready);
		FD_SET(selfsock, &ready);
		rv = select(selfsock+1, &ready, NULL, NULL, &tv);
		if (rv < 0) {
			if (errno == EINTR)
				return -1;
			FrisPfatal("PacketRequest(select)");
		}
		if (rv == 0)
			return -1;
	}
#endif
	alen = sizeof(from);
	bzero(&from, alen);
	if ((mlen = recvfrom(selfsock, p, sizeof(*p), 0,
			     (struct sockaddr *)&from, &alen)) < 0) {
		if (errno == EWOULDBLOCK || errno == EINTR)
			return -1;
		FrisPfatal("PacketRequest(recvfrom)");
	}

	/*
	 * Basic integrity checks
	 */
	if (mlen < sizeof(p->hdr) + p->hdr.datalen) {
		FrisLog("Bad message length (%d != %d)",
			mlen, p->hdr.datalen);
		return 1;
	}
#ifdef SAME_HOST_HACK
	/*
	 * If using a host alias for the client, a message may get
	 * the wrong IP, so rig the IP check to make it always work.
	 */
	if (p->hdr.srcip != from.sin_addr.s_addr)
		from.sin_addr.s_addr = p->hdr.srcip;

	/*
	 * Also, we aren't binding to a port on the client side, so the
	 * first message to the server will contain the actual port we
	 * will use from now on.
	 */
	if (!isclient && sndportnum == htons(portnum) &&
	    sndportnum != from.sin_port)
		sndportnum = from.sin_port;
#endif

  /*SWP2P - Temp - Commenting the below section to solve the below problem*/
  /*Packet's source is changed to switch IP when the packet is sent out.*/
#if 0 /*SWP2P - Temp - Start*/
	if (p->hdr.srcip != from.sin_addr.s_addr) {
		FrisLog("Bad message source (%x != %x)",
			ntohl(from.sin_addr.s_addr), ntohl(p->hdr.srcip));
		return 1;
	}

	if (sndportnum != from.sin_port) {
		FrisLog("Bad message port (%d != %d)",
			ntohs(from.sin_port), ntohs(sndportnum));
		return 1;
	}
#endif /*SWP2P - Temp - End*/
	return 0;
}
#endif

#ifndef MSG_DONTWAIT
#define MSG_DONTWAIT 0
#endif

/*
 * We use blocking sends since there is no point in giving up. All packets
 * go to the same place, whether client or server.
 *
 * The amount of data sent is determined from the datalen of the packet hdr.
 * All packets are actually the same size/structure. 
 */
void
swp2p_PacketSend(Packet_t *p, int *resends, struct in_addr client_ipaddr, Swp2pPeerStats_t *peer)
{
	struct sockaddr_in to;
	int		   len, delays, rc;
	int		   fd = sock;

  if (peer != NULL)
  {
    fd = peer->psock;
  }
	len = sizeof(p->hdr) + p->hdr.datalen;
	p->hdr.srcip = myipaddr.s_addr;

	to.sin_family      = AF_INET;
	to.sin_port        = sndportnum;
	to.sin_addr.s_addr = client_ipaddr.s_addr;

  /*SWP2P - Temp - Start*/
  #if 0
  struct in_addr unicast_dest_addr;
  inet_aton("20.1.1.1", &unicast_dest_addr);
  to.sin_addr.s_addr = unicast_dest_addr.s_addr;
  #endif
  /*SWP2P - Temp - End*/

	delays = 0;
#ifdef USE_REUSEADDR_COMPAT
	/* send out selfsock so the source IP is ours and not the MC addr */
	if (selfsock >= 0)
		fd = selfsock;
#endif
	while ((rc = sendto(fd, (void *)p, len, MSG_DONTWAIT,
			    (struct sockaddr *)&to, sizeof(to))) <= 0) {
		if (rc < 0 && !(errno == ENOBUFS || errno == EAGAIN))
			FrisPfatal("PacketSend(sendto)");

		/*
		 * ENOBUFS (BSD) or EAGAIN (Linux, because we set DONTWAIT)
		 * means there was not enough socket space for the packet.
		 * Okay to sleep a bit to let things drain.
		 *
		 * Note that on BSD, ENOBUFS is also returned when the NIC
		 * send buffers are full, so we should never lose a packet
		 * on the send path.
		 *
		 * On Linux, we get this behavior as well by turning on
		 * the extended error message passing (IP_RECVERR).
		 */
		delays++;
		fsleep(nobufdelay);
	}

	DOSTAT(nonetbufs += delays);
	if (resends != 0)
		*resends = delays;
}
void
tcp_swp2p_PacketSend(Packet_t *p, int *resends, struct in_addr client_ipaddr, Swp2pPeerStats_t *peer)
{
	struct sockaddr_in to;
	int		   len, delays, rc;
	int		   fd = sock;

  if (peer != NULL)
  {
    fd = peer->psock;
  }
	len = sizeof(p->hdr) + p->hdr.datalen;
	p->hdr.srcip = myipaddr.s_addr;

  send (peer->psock, (void *)p, len, 0);
	to.sin_family      = AF_INET;
	to.sin_port        = sndportnum;
	to.sin_addr.s_addr = client_ipaddr.s_addr;

  /*SWP2P - Temp - Start*/
  #if 0
  struct in_addr unicast_dest_addr;
  inet_aton("20.1.1.1", &unicast_dest_addr);
  to.sin_addr.s_addr = unicast_dest_addr.s_addr;
  #endif
  /*SWP2P - Temp - End*/
	delays = 0;
#if 0
#ifdef USE_REUSEADDR_COMPAT
	/* send out selfsock so the source IP is ours and not the MC addr */
	if (selfsock >= 0)
		fd = selfsock;
#endif
	while ((rc = sendto(fd, (void *)p, len, MSG_DONTWAIT,
			    (struct sockaddr *)&to, sizeof(to))) <= 0) {
		if (rc < 0 && !(errno == ENOBUFS || errno == EAGAIN))
			FrisPfatal("PacketSend(sendto)");

		/*
		 * ENOBUFS (BSD) or EAGAIN (Linux, because we set DONTWAIT)
		 * means there was not enough socket space for the packet.
		 * Okay to sleep a bit to let things drain.
		 *
		 * Note that on BSD, ENOBUFS is also returned when the NIC
		 * send buffers are full, so we should never lose a packet
		 * on the send path.
		 *
		 * On Linux, we get this behavior as well by turning on
		 * the extended error message passing (IP_RECVERR).
		 */
		delays++;
		fsleep(nobufdelay);
	}
#endif
	DOSTAT(nonetbufs += delays);
	if (resends != 0)
		*resends = delays;
}

void
PacketSend(Packet_t *p, int *resends)
{
	struct sockaddr_in to;
	int		   len, delays, rc;
	int		   fd = sock;

	len = sizeof(p->hdr) + p->hdr.datalen;
	p->hdr.srcip = myipaddr.s_addr;

	to.sin_family      = AF_INET;
	to.sin_port        = sndportnum;
	to.sin_addr.s_addr = mcastaddr.s_addr;

  /*SWP2P - Temp - Start*/
  #if 0
  struct in_addr unicast_dest_addr;
  inet_aton("20.1.1.1", &unicast_dest_addr);
  to.sin_addr.s_addr = unicast_dest_addr.s_addr;
  #endif /*if 0*/
  /*SWP2P - Temp - End*/

	delays = 0;
#ifdef USE_REUSEADDR_COMPAT
	/* send out selfsock so the source IP is ours and not the MC addr */
	if (selfsock >= 0)
		fd = selfsock;
#endif
	while ((rc = sendto(fd, (void *)p, len, MSG_DONTWAIT,
			    (struct sockaddr *)&to, sizeof(to))) <= 0) {
		if (rc < 0 && !(errno == ENOBUFS || errno == EAGAIN))
			FrisPfatal("PacketSend(sendto)");

		/*
		 * ENOBUFS (BSD) or EAGAIN (Linux, because we set DONTWAIT)
		 * means there was not enough socket space for the packet.
		 * Okay to sleep a bit to let things drain.
		 *
		 * Note that on BSD, ENOBUFS is also returned when the NIC
		 * send buffers are full, so we should never lose a packet
		 * on the send path.
		 *
		 * On Linux, we get this behavior as well by turning on
		 * the extended error message passing (IP_RECVERR).
		 */
		delays++;
		fsleep(nobufdelay);
	}

	DOSTAT(nonetbufs += delays);
	if (resends != 0)
		*resends = delays;
}

/*
 * Basically the same as above, but instead of sending to the multicast
 * group, send to the (unicast) IP in the packet header. This simplifies
 * the logic in a number of places, by avoiding having to deal with
 * multicast packets that are not destined for us, but for someone else.
 */
void
PacketReply(Packet_t *p, int firenforget)
{
	struct sockaddr_in to;
	int		len;
	int		fd = sock;

	len = sizeof(p->hdr) + p->hdr.datalen;

	to.sin_family      = AF_INET;
	to.sin_port        = sndportnum;
	to.sin_addr.s_addr = p->hdr.srcip;
	p->hdr.srcip       = myipaddr.s_addr;

#ifdef USE_REUSEADDR_COMPAT
	/* send out selfsock so the source IP is ours and not the MC addr */
	if (selfsock >= 0)
		fd = selfsock;
#endif

	while (sendto(fd, (void *)p, len, 0, 
		      (struct sockaddr *)&to, sizeof(to)) < 0) {
		if (errno != ENOBUFS && errno != EAGAIN)
			FrisPfatal("PacketReply(sendto)");

		if (firenforget)
			break;

		/*
		 * ENOBUFS means we ran out of mbufs. Okay to sleep a bit
		 * to let things drain.
		 */
		DOSTAT(nonetbufs++);
		fsleep(nobufdelay);
	}
}

void *
PeerWorkerThread (void *arg)
{
  Swp2pPeerStats_t *peer = (Swp2pPeerStats_t *)arg;
  SQelem_t *sqel;
	Packet_t	packet, *p = &packet;
  int resend = 0;
  int rv = 0;
  struct timeval temp_transfer_start, temp_transfer_end;

  while (1)
  {
    gettimeofday (&temp_transfer_start, 0);
    if (peer->stop_worker == 1)
    {
      break;
    }

    pthread_mutex_lock(&(peer->SendQLock));
    /*Check if the Queue is empty*/
    if (queue_empty (&(peer->SendQ)))
    {
      /*If the Queue is empty then wait for the Cond Signal or Timeout*/
      rv = pthread_cond_timedwait (&(peer->SendQCond),
                                   &(peer->SendQLock),
                                   &(peer->SendQTimespec));
      if (rv != 0)
      {
        assert(rv == ETIMEDOUT);
        pthread_mutex_unlock(&(peer->SendQLock));
        continue;
      }
      assert(!queue_empty(&(peer->SendQ)));
    }

    /*Get it from Queue, make a copy and then Free it*/
    sqel = (SQelem_t *) queue_first (&(peer->SendQ));
    memcpy (p, &(sqel->p), sizeof (Packet_t));
    resend = sqel->resends;
    queue_remove(&(peer->SendQ), sqel, SQelem_t *, chain); 
    free (sqel);
    sqel = NULL;
    pthread_mutex_unlock(&(peer->SendQLock));

    /*Send it out*/
	  tcp_swp2p_PacketSend(p, &resend, peer->peer_ipaddr, peer);
    no_of_blocks_served ++;
    /*Sleep for required time*/
    gettimeofday (&temp_transfer_end, 0);
    timersub (&temp_transfer_end, &temp_transfer_start, &temp_transfer_end);
    timeradd (&temp_transfer_end, &(peer->sending_time_spent), &(peer->sending_time_spent));
  }
}

void
add_pkt_to_peer_sendq (Packet_t *p_orig, int resend, struct in_addr ipaddr)
{
  Swp2pPeerStats_t *peer = get_peer_stat_struct (ipaddr);
  struct timeval temp_transfer_start, temp_transfer_end;

  gettimeofday (&temp_transfer_start, 0);

  if (peer->send_tid == 0)
  {
    if (pthread_create(&(peer->send_tid), NULL, PeerWorkerThread, (void *)peer))
    {
      FrisFatal("Failed to create Peer worker thread!");
    }
    else
    {
      if (nw_debug > 0)
      {
        FrisLog ("Created PeerWorkerThread with Id %lu ", peer->send_tid);
      }
    }
  }

  SQelem_t *sqel = NULL;
   
  if (peer == NULL)
   {
     return;
   }

  /*Create Send Q element*/
  sqel = calloc (1, sizeof (SQelem_t));
  if (sqel == NULL)
  {
    FrisFatal("sqel allocation failled - No more memory");
  }
  sqel->resends = resend;
  memcpy (&(sqel->p), p_orig, sizeof (Packet_t));
  
  /*Add it to Send Q*/
  pthread_mutex_lock (&(peer->SendQLock));

  queue_enter(&(peer->SendQ), sqel, SQelem_t *, chain);

#ifdef CONDVARS_WORK
  pthread_cond_signal(&(peer->SendQCond));
#endif  
  pthread_mutex_unlock (&(peer->SendQLock));

  gettimeofday (&temp_transfer_end, 0);
  timersub (&temp_transfer_end, &temp_transfer_start, &temp_transfer_end);
  timeradd (&temp_transfer_end, &(peer->sending_time_spent), &(peer->sending_time_spent));
}

int
PacketValid(Packet_t *p, int nchunks)
{
	switch (p->hdr.type) {
	case PKTTYPE_REQUEST:
	case PKTTYPE_REPLY:
		break;
	default:
		return 0;
	}

	switch (p->hdr.subtype) {
	case PKTSUBTYPE_BLOCK:
		if (p->hdr.datalen < sizeof(p->msg.block))
			return 0;
		if (p->msg.block.chunk < 0 ||
		    p->msg.block.chunk >= nchunks ||
		    p->msg.block.block < 0 ||
		    p->msg.block.block >= MAXCHUNKSIZE)
			return 0;
		break;
	case PKTSUBTYPE_REQUEST:
		if (p->hdr.datalen < sizeof(p->msg.request))
			return 0;
		if (p->msg.request.chunk < 0 ||
		    p->msg.request.chunk >= nchunks ||
		    p->msg.request.block < 0 ||
		    p->msg.request.block >= MAXCHUNKSIZE ||
		    p->msg.request.count < 0 ||
		    p->msg.request.block+p->msg.request.count > MAXCHUNKSIZE)
			return 0;
		break;
	case PKTSUBTYPE_PREQUEST:
		if (p->hdr.datalen < sizeof(p->msg.prequest))
			return 0;
		if (p->msg.prequest.chunk < 0 ||
		    p->msg.prequest.chunk >= nchunks)
			return 0;
		break;
	case PKTSUBTYPE_JOIN:
		if (p->hdr.datalen < sizeof(p->msg.join))
			return 0;
		break;
	case PKTSUBTYPE_JOIN2:
		if (p->hdr.datalen < sizeof(p->msg.join2))
			return 0;
		break;
	case PKTSUBTYPE_LEAVE:
		if (p->hdr.datalen < sizeof(p->msg.leave))
			return 0;
		break;
	case PKTSUBTYPE_LEAVE2:
		if (p->hdr.datalen < sizeof(p->msg.leave2))
			return 0;
		break;
	case PKTSUBTYPE_PROGRESS:
		if (p->hdr.datalen < sizeof(p->msg.progress.hdr))
			return 0;
		break;
	default:
		return 0;
	}

	return 1;
}

/*
 * Functions for communicating with the master server.
 *
 * TODO: protocol for negotiating the protocol version:
 * On the client, send a request with our current version and:
 *   - get a version error back: server must be V01, so redo with V01
 *   - otherwise header reply contains version
 *     if not our version, must be a lower version, so redo with that version
 * On the server:
 *   - version less than our current version, use that version
 *   - version greater than ours, reply with our version 
 */
#ifdef MASTER_SERVER
int
MsgSend(int msock, MasterMsg_t *msg, size_t size, int timo)
{
	void *buf = msg;
	int cc;
	struct timeval tv, now, then;
	fd_set wfds;

	if (timo) {
		tv.tv_sec = timo;
		tv.tv_usec = 0;
		gettimeofday(&then, NULL);
		timeradd(&then, &tv, &then);
	}
	while (size > 0) {
		if (timo) {
			gettimeofday(&now, NULL);
			if (timercmp(&now, &then, >=)) {
				cc = 0;
			} else {
				timersub(&then, &now, &tv);
				FD_ZERO(&wfds);
				FD_SET(msock, &wfds);
				cc = select(msock+1, NULL, &wfds, NULL, &tv);
			}
			if (cc <= 0) {
				if (cc == 0) {
					errno = ETIMEDOUT;
					cc = -1;
				}
				break;
			}
		}

		cc = write(msock, buf, size);
		if (cc <= 0)
			break;

		size -= cc;
		buf += cc;
	}

	if (size != 0) {
		char *estr = "master server message send";
		if (cc == 0)
			fprintf(stderr, "%s: Unexpected EOF\n", estr);
		else
			perror(estr);
		return 0;
	}
	return 1;
}

int
MsgReceive(int msock, MasterMsg_t *msg, size_t size, int timo)
{
	void *buf = msg;
	int cc;
	struct timeval tv, now, then;
	fd_set rfds;

	if (timo) {
		tv.tv_sec = timo;
		tv.tv_usec = 0;
		gettimeofday(&then, NULL);
		timeradd(&then, &tv, &then);
	}
	while (size > 0) {
		if (timo) {
			gettimeofday(&now, NULL);
			if (timercmp(&now, &then, >=)) {
				cc = 0;
			} else {
				timersub(&then, &now, &tv);
				FD_ZERO(&rfds);
				FD_SET(msock, &rfds);
				cc = select(msock+1, &rfds, NULL, NULL, &tv);
			}
			if (cc <= 0) {
				if (cc == 0) {
					errno = ETIMEDOUT;
					cc = -1;
				}
				break;
			}
		}

		cc = read(msock, buf, size);
		if (cc <= 0)
			break;

		size -= cc;
		buf += cc;
	}

	if (size != 0) {
		char *estr = "master server message receive";
		if (cc == 0)
			fprintf(stderr, "%s: Unexpected EOF\n", estr);
		else
			perror(estr);
		return 0;
	}
	return 1;
}

/*
 * Contact the master server to discover download information for imageid.
 * 'sip' and 'sport' are the addr/port of the master server, 'method'
 * specifies the desired download method, 'askonly' is set to just ask
 * for information about the image (without starting a server), 'timeout'
 * is how long to wait for a response.
 *
 * If 'hostip' is not zero, then we are requesting information on behalf of
 * that node.  The calling node (us) must have "proxy" permission on the
 * server for this to work.
 *
 * On success, return non-zero with 'reply' filled in with the server's
 * response IN HOST ORDER.  On failure returns zero.
 */
int
ClientNetFindServer(in_addr_t sip, in_port_t sport,
		    in_addr_t hostip, char *imageid,
		    int method, int askonly, int timeout,
		    GetReply *reply, struct in_addr *myip)
{
	struct sockaddr_in name;
	MasterMsg_t msg;
	int msock, len;
	
	if ((msock = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0) {
		perror("Could not allocate socket for master server");
		return 0;
	}
	if (sport == 0)
		sport = MS_PORTNUM;

	/* XXX need connection timeout! */

	name.sin_family = AF_INET;
	name.sin_addr.s_addr = htonl(sip);
	name.sin_port = htons(sport);
	if (connect(msock, (struct sockaddr *)&name, sizeof(name)) < 0) {
		fprintf(stderr,
			"Connecting to master server %s:%d failed: %s",
			inet_ntoa(name.sin_addr), sport, strerror(errno));
		close(msock);
		return 0;
	}

	/*
	 * XXX recover the IP address of the interface used to talk to
	 * the server.
	 */
	if (myip) {
		struct sockaddr_in me;
		socklen_t len = sizeof me;

		if (getsockname(msock, (struct sockaddr *)&me, &len) < 0) {
			perror("getsockname");
			close(msock);
			return 0;
		}
		*myip = me.sin_addr;
	}

	memset(&msg, 0, sizeof msg);
	strncpy((char *)msg.hdr.version, MS_MSGVERS_1,
		sizeof(msg.hdr.version));
	msg.hdr.type = htonl(MS_MSGTYPE_GETREQUEST);
	msg.body.getrequest.hostip = htonl(hostip);
	if (askonly) {
		msg.body.getrequest.status = 1;
		msg.body.getrequest.methods = MS_METHOD_ANY;
	} else {
		msg.body.getrequest.methods = method;
	}
	len = strlen(imageid);
	if (len > MS_MAXIDLEN)
		len = MS_MAXIDLEN;
	msg.body.getrequest.idlen = htons(len);
	strncpy((char *)msg.body.getrequest.imageid, imageid, MS_MAXIDLEN);

	len = sizeof msg.hdr + sizeof msg.body.getrequest;
	if (!MsgSend(msock, &msg, len, timeout)) {
		close(msock);
		return 0;
	}

	memset(&msg, 0, sizeof msg);
	len = sizeof msg.hdr + sizeof msg.body.getreply;
	if (!MsgReceive(msock, &msg, len, timeout)) {
		close(msock);
		return 0;
	}
	close(msock);

	if (strncmp((char *)msg.hdr.version, MS_MSGVERS_1,
		    sizeof(msg.hdr.version))) {
		fprintf(stderr,
			"Got incorrect version from master server %s:%d\n",
			inet_ntoa(name.sin_addr), sport);
		return 0;
	}
	if (ntohl(msg.hdr.type) != MS_MSGTYPE_GETREPLY) {
		fprintf(stderr,
			"Got incorrect reply from master server %s:%d\n",
			inet_ntoa(name.sin_addr), sport);
		return 0;
	}

	/*
	 * Convert the reply info to host order
	 */
	*reply = msg.body.getreply;
	reply->error = ntohs(reply->error);
	reply->servaddr = ntohl(reply->servaddr);
	reply->addr = ntohl(reply->addr);
	reply->port = ntohs(reply->port);
	reply->sigtype = ntohs(reply->sigtype);
	if (reply->sigtype == MS_SIGTYPE_MTIME)
		*(uint32_t *)reply->signature =
			ntohl(*(uint32_t *)reply->signature);
	reply->hisize = ntohl(reply->hisize);
	reply->losize = ntohl(reply->losize);
	return 1;
}

/*
 * Contact the master server to negotiate an upload for a 'file' to store
 * under the given 'imageid'.
 *
 * 'sip' and 'sport' are the addr/port of the master server, 'askonly' is
 * set to just see if the upload is allowed and to get characteristics of
 * any existing copy of the image, 'timeout' is how long to wait for a
 * response.
 *
 * If 'hostip' is not zero, then we are requesting information on behalf of
 * that node.  The calling node (us) must have "proxy" permission on the
 * server for this to work.
 *
 * On success, return non-zero with 'reply' filled in with the server's
 * response IN HOST ORDER.  On failure returns zero.
 */
int
ClientNetPutRequest(in_addr_t sip, in_port_t sport,
		    in_addr_t hostip, char *imageid,
		    uint64_t isize, uint32_t mtime,
		    int timeout, int askonly, int reqtimo, PutReply *reply)
{
	struct sockaddr_in name;
	MasterMsg_t msg;
	int msock, len;
	
	if ((msock = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0) {
		perror("Could not allocate socket for master server");
		return 0;
	}
	if (sport == 0)
		sport = MS_PORTNUM;

	/* XXX need connection timeout! */

	name.sin_family = AF_INET;
	name.sin_addr.s_addr = htonl(sip);
	name.sin_port = htons(sport);
	if (connect(msock, (struct sockaddr *)&name, sizeof(name)) < 0) {
		fprintf(stderr,
			"Connecting to master server %s:%d failed: %s\n",
			inet_ntoa(name.sin_addr), sport, strerror(errno));
		close(msock);
		return 0;
	}

	memset(&msg, 0, sizeof msg);
	strncpy((char *)msg.hdr.version, MS_MSGVERS_1,
		sizeof(msg.hdr.version));
	msg.hdr.type = htonl(MS_MSGTYPE_PUTREQUEST);
	msg.body.putrequest.hostip = htonl(hostip);
	if (askonly)
		msg.body.putrequest.status = 1;
	len = strlen(imageid);
	if (len > MS_MAXIDLEN)
		len = MS_MAXIDLEN;
	msg.body.putrequest.idlen = htons(len);
	strncpy((char *)msg.body.putrequest.imageid, imageid, MS_MAXIDLEN);
	if (isize > 0) {
		msg.body.putrequest.hisize = htonl(isize >> 32);
		msg.body.putrequest.losize = htonl(isize);
	}
	if (mtime)
		msg.body.putrequest.mtime = htonl(mtime);
	/* XXX have the server wait longer than us so we timeout first */
	if (timeout)
		msg.body.putrequest.timeout = htonl(timeout+2);

	len = sizeof msg.hdr + sizeof msg.body.putrequest;
	if (!MsgSend(msock, &msg, len, reqtimo)) {
		close(msock);
		return 0;
	}

	memset(&msg, 0, sizeof msg);
	len = sizeof msg.hdr + sizeof msg.body.putreply;
	if (!MsgReceive(msock, &msg, len, reqtimo)) {
		close(msock);
		return 0;
	}
	close(msock);

	if (strncmp((char *)msg.hdr.version, MS_MSGVERS_1,
		    sizeof(msg.hdr.version))) {
		fprintf(stderr,
			"Got incorrect version from master server %s:%d\n",
			inet_ntoa(name.sin_addr), sport);
		return 0;
	}
	if (ntohl(msg.hdr.type) != MS_MSGTYPE_PUTREPLY) {
		fprintf(stderr,
			"Got incorrect reply from master server %s:%d\n",
			inet_ntoa(name.sin_addr), sport);
		return 0;
	}

	/*
	 * Convert the reply info to host order
	 */
	*reply = msg.body.putreply;
	reply->error = ntohs(reply->error);
	reply->addr = ntohl(reply->addr);
	reply->port = ntohs(reply->port);
	reply->sigtype = ntohs(reply->sigtype);
	if (reply->sigtype == MS_SIGTYPE_MTIME)
		*(uint32_t *)reply->signature =
			ntohl(*(uint32_t *)reply->signature);
	reply->hisize = ntohl(reply->hisize);
	reply->losize = ntohl(reply->losize);
	reply->himaxsize = ntohl(reply->himaxsize);
	reply->lomaxsize = ntohl(reply->lomaxsize);
	return 1;
}

#endif

/*
 * Functions for dealing with IGMP
 */
#ifdef WITH_IGMP
#include <netinet/in_systm.h>	/* for older *BSD that needs n_long */
#include <netinet/ip.h>
#include <netinet/igmp.h>

#ifdef IGMP_MEMBERSHIP_QUERY
#define IGMP_QUERY IGMP_MEMBERSHIP_QUERY
#else
#define IGMP_QUERY IGMP_HOST_MEMBERSHIP_QUERY
#endif
#ifdef IGMP_V2_MEMBERSHIP_REPORT
#define IGMP_REPORT IGMP_V2_MEMBERSHIP_REPORT
#else
#define IGMP_REPORT IGMP_v2_HOST_MEMBERSHIP_REPORT
#endif

static struct igmp qpacket, rpacket;
static struct in_addr mciface;
static struct sockaddr_in allhosts, mcgroup;

static uint16_t
igmp_csum(struct igmp *pkt)
{
	char *addr = (char *)pkt;
	int cc = sizeof(*pkt);
	uint32_t csum = 0;

	while (cc >= sizeof(uint16_t)) {
		csum += *(uint16_t *)addr;
		addr += sizeof(uint16_t);
		cc -= sizeof(uint16_t);
	}
	if (cc > 0)
		csum = csum + *(uint8_t *)addr;

	while ((csum >> 16) != 0)
		csum = (csum >> 16) + (csum & 0xFFFF);

	return(~csum);
}

static int
igmp_opensocket(void)
{
	char ra[4];
	int ttl = 1;
	int sock;

	sock = socket(AF_INET, SOCK_RAW, IPPROTO_IGMP);
	if (sock < 0) {
		perror("IGMP socket");
		return -1;
	}
	
	/* set TTL */
	if (setsockopt(sock, IPPROTO_IP, IP_MULTICAST_TTL,
		       &ttl, sizeof(ttl)) < 0) {
		perror("setsockopt(MULTICAST_TTL)");
		close(sock);
		return -1;
	}

	/* fix interface */
	if (mciface.s_addr != 0 &&
	    setsockopt(sock, IPPROTO_IP, IP_MULTICAST_IF,
		       &mciface, sizeof(mciface)) < 0) {
		perror("setsockopt(MULTICAST_IF)");
		close(sock);
		return -1;
	}

	/* set router alert option */
	ra[0] = IPOPT_RA;
	ra[1] = 4;
	ra[2] = ra[3] = '\0';
	if (setsockopt(sock, IPPROTO_IP, IP_OPTIONS, &ra, sizeof(ra)) < 0) {
		perror("setsockopt(RA)");
		close(sock);
		return -1;
	}

	return sock;
}

void
IGMPInit(struct in_addr *iface, struct in_addr *mcaddr)
{
	/* build a prototype query packet */
	qpacket.igmp_type = IGMP_QUERY;
	qpacket.igmp_code = 0x64;
	memset(&qpacket.igmp_group, 0, sizeof(qpacket.igmp_group));
	qpacket.igmp_cksum = 0;
	qpacket.igmp_cksum = igmp_csum(&qpacket);

	/* sockaddr for queries */
	allhosts.sin_family = AF_INET;
	allhosts.sin_port = htons(0);
	allhosts.sin_addr.s_addr = htonl(INADDR_ALLHOSTS_GROUP);

	if (mcaddr != NULL) {
		/* build a prototype report packet */
		rpacket.igmp_type = IGMP_REPORT;
		rpacket.igmp_code = 0;
		rpacket.igmp_group = *mcaddr;
		rpacket.igmp_cksum = 0;
		rpacket.igmp_cksum = igmp_csum(&rpacket);

		/* sockaddr for reports */
		mcgroup.sin_family = AF_INET;
		mcgroup.sin_port = htons(0);
		mcgroup.sin_addr = *mcaddr;
	}

	/* remember the interface */
	if (iface != NULL)
		mciface = *iface;
	else
		mciface.s_addr = 0;
}

int
IGMPSendQuery(void)
{
	int rv, sock;

	if ((sock = igmp_opensocket()) < 0)
		return -1;

	rv = sendto(sock, &qpacket, sizeof(qpacket), 0,
		  (struct sockaddr *)&allhosts, sizeof(allhosts));
	if (rv < 0)
		perror("query sendto");
	close(sock);

	return (rv != sizeof(qpacket));
}

int
IGMPSendReport(void)
{
	int rv, sock;

	if (mcgroup.sin_addr.s_addr == 0)
		return 0;

	if ((sock = igmp_opensocket()) < 0)
		return -1;

	rv = sendto(sock, &rpacket, sizeof(rpacket), 0,
		    (struct sockaddr *)&mcgroup, sizeof(mcgroup));
	if (rv < 0)
		perror("report sendto");
	close(sock);

	return (rv != sizeof(rpacket));
}
#endif
